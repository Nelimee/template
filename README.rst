Template for quantum projects at Total
======================================



Useful links:
-------------

- `Official Python packaging user guide <https://packaging.python.org/>`_.
- `Sample project from setuptools <https://github.com/pypa/sampleproject>`_.
- `ReStructuredText basics <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_.
- `Sphinx documentation <https://www.sphinx-doc.org/en/master/>`_.
